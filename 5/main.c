//2015110705 김민규
//본인은 이 소스파일을 다른 사람의 소스를 복사하지 않고 직접 작성하였습니다.

#include <stdio.h>
#include <stdlib.h>

#define MAX_SIZE 4

void input(FILE*, int[][MAX_SIZE]);
int add(int[][MAX_SIZE], int[][MAX_SIZE], int c[][MAX_SIZE], int rows, int cols);

int main()
{
	int a[3][4];
	int b[3][4];
	int c[3][4];

	FILE* fp1 = fopen("a.txt", "r");
	FILE* fp2 = fopen("b.txt", "r");
	
	input(fp1, a);
	input(fp2, b);

	printf("program step count : %d\n", add(a, b, c, 3, 4));

	fclose(fp1);
	fclose(fp2);

	return 0;
}

void input(FILE* _fp, int _arr[][4])
{
	int i, j;
	int buffer;

	for(i = 0; i < 3; i++)
	{
		for(j = 0; j < 4; j++)
		{
			fscanf(_fp, "%d", &buffer);
			_arr[i][j] = buffer;
		}
	}
}

int add(int a[][MAX_SIZE], int b[][MAX_SIZE], int c[][MAX_SIZE], int rows, int cols)
{
	int i, j;
	int count = 0;

	for(i = 0; i < rows; i++)
	{
		for(j = 0; j < cols; j++)
		{
			c[i][j] = a[i][j] + b[i][j];
			count += 2;
		}
		count +=2;
	}
	count++;

	return count;
}