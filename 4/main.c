//2015110705 김민규
//본인은 이 소스파일을 다른 사람의 소스를 복사하지 않고 직접 작성하였습니다.

#include <stdio.h>
#include <stdlib.h>

void input(int*, int, FILE*);
void increase(int*, int);
void output(int *, int, FILE*);

int main()
{
	int num, *pNum;
	
	FILE* fpIn = fopen("input.txt", "r");
	FILE* fpOut = fopen("output.txt", "w");

	fscanf(fpIn, "%d", &num);
	pNum = (int*)malloc(sizeof(int) * num);
	input(pNum, num, fpIn);
	increase(pNum, num);
	output(pNum, num, fpOut);

	free(pNum);
	fclose(fpIn);
	fclose(fpOut);

	return 0;
}

void input(int* _arr, int _count, FILE* _fp)
{
	int i;
	int buffer;

	for(i = 0; i < _count; i++)
	{
		fscanf(_fp, "%d", &buffer);
		_arr[i] = buffer;
	}
}

//모든 배열 1씩 증가
void increase(int* _arr, int _num)
{
	int i;

	for(i = 0; i < _num; i++)
		_arr[i]++;
}

void output(int *_arr, int _num, FILE* _fp)
{
	int i;

	fprintf(_fp, "%d\n", _num);

	for(i = 0; i < _num; i++)
	{
		fprintf(_fp, "%d ", _arr[i]);
	}

}