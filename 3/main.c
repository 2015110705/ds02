//2015110705 김민규
//본인은 이 소스파일을 다른 사람의 소스를 복사하지 않고 직접 작성하였습니다.

#include <stdio.h>
#include <stdlib.h>

int *func1();
int *func2(int*);
int **func3(int**);

int main()
{
	int *pNum = func1();
	printf("%d\n", *pNum);
	printf("%d\n", *func2(pNum));
	printf("%d\n", **func3(&pNum));
	free(pNum);
	return 0;
}

int *func1()
{
	int* pReturn = (int*)malloc(sizeof(int));

	*pReturn = 0;

	return pReturn;
}

int *func2(int *_pNum)
{
	*_pNum = 100;

	return _pNum;
}

int **func3(int** _ppNum)
{
	**_ppNum = 200;

	return _ppNum;
}