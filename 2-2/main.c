//2015110705 김민규
//본인은 이 소스파일을 다른 사람의 소스를 복사하지 않고 직접 작성하였습니다.

#include <stdio.h>
#include <stdlib.h>

int* func(int);

int main()
{
	int num = 1;
	int *pNum = func(num);
	printf("%d\n", num);
	free(pNum);

	return 0;
}

int* func(int _num)
{
	int *pNum = (int*)malloc(sizeof(int));
	*pNum = _num;
	printf("%d\n", *pNum);
	return pNum;
}